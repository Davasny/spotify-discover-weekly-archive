# spotify-discover-weekly-archive

gpt4 chat: https://chat.openai.com/share/ff679ff1-3e99-41f0-b646-06e18a477395

```
1. Use the Spotify API to get the songs from the Discover Weekly playlist using Python and JavaScript.
2. Add the songs from the Discover Weekly playlist to another playlist named "Discover Weekly Archive". If this playlist doesn't exist, create a new one.
3. Check if a song is already added to the "Discover Weekly Archive" playlist before adding it.
4. Store the Spotify app credentials (client ID and client secret) in environment variables instead of hardcoding them in the script.
5. Read the Spotify cache file from an environment variable.
6. Use an Object-Oriented Programming (OOP) design for the Python script.
7. Make the Python script compatible with running as a GitLab CI/CD pipeline job, including reading the Spotify refresh token from an environment variable.
8. Write a `.gitlab-ci.yml` file for the GitLab CI/CD pipeline job.
9. Run the pipeline job as a cron job using GitLab's pipeline schedules.
10. Add the option to run the GitLab CI/CD job manually.
11. Handle an error in refreshing the Spotify access token using the stored refresh token.
12. Add print statements to the Python script to show progress and actions, including found songs and actions taken (or skipped).
```
