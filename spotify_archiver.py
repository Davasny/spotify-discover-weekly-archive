import os
import spotipy
from spotipy.oauth2 import SpotifyOAuth

class SpotifyArchiver:
    def __init__(self):
        oauth_manager = SpotifyOAuth(
            client_id=os.getenv('SPOTIPY_CLIENT_ID'),
            client_secret=os.getenv('SPOTIPY_CLIENT_SECRET'),
            redirect_uri=os.getenv('SPOTIPY_REDIRECT_URI'),
            scope='user-library-read playlist-read-private playlist-modify-public playlist-modify-private',
            cache_path=None)

        refresh_token = os.getenv('SPOTIPY_REFRESH_TOKEN')
        new_token_info = oauth_manager.refresh_access_token(refresh_token)

        self.sp = spotipy.Spotify(auth_manager=oauth_manager)
        self.sp.token_info = new_token_info
        self.user_id = self.sp.me()['id']

    def get_playlist_by_name(self, name):
        playlists = self.sp.current_user_playlists()['items']
        return next((playlist for playlist in playlists if playlist['name'] == name), None)

    def create_playlist(self, name):
        print(f'Creating new playlist: {name}')
        return self.sp.user_playlist_create(self.user_id, name)

    def add_track_to_playlist(self, playlist_id, track_id):
        print(f'Adding track {track_id} to playlist {playlist_id}')
        self.sp.playlist_add_items(playlist_id, [track_id])

    def archive_discover_weekly(self):
        discover_weekly = self.get_playlist_by_name('Discover Weekly')
        archive = self.get_playlist_by_name('Discover Weekly Archive')

        if archive is None:
            archive = self.create_playlist('Discover Weekly Archive')

        if discover_weekly is not None:
            tracks = self.sp.playlist_items(discover_weekly['id'])['items']

            for track in tracks:
                track_id = track['track']['id']
                track_name = track['track']['name']
                print(f'Found track {track_name} ({track_id}) in Discover Weekly')

                archive_tracks = self.sp.playlist_items(archive['id'])['items']

                if any(archive_track['track']['id'] == track_id for archive_track in archive_tracks):
                    print(f'Track {track_name} ({track_id}) is already in the archive, skipping')
                else:
                    self.add_track_to_playlist(archive['id'], track_id)
        else:
            print("Could not find a Discover Weekly playlist.")


if __name__ == '__main__':
    archiver = SpotifyArchiver()
    archiver.archive_discover_weekly()
